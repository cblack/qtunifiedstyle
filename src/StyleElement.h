/*
 * SPDX-FileCopyrightText: 2020 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#ifndef STYLEDELEMENT_H
#define STYLEDELEMENT_H

#include <memory>
#include <optional>

#include <QObject>
#include <QColor>
#include <QFont>

class Q_DECL_EXPORT StyleElement : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QColor color READ color NOTIFY updated)
    Q_PROPERTY(QColor backgroundColor READ backgroundColor NOTIFY updated)

    Q_PROPERTY(qreal leftPadding READ leftPadding NOTIFY updated)
    Q_PROPERTY(qreal rightPadding READ rightPadding NOTIFY updated)
    Q_PROPERTY(qreal topPadding READ topPadding NOTIFY updated)
    Q_PROPERTY(qreal bottomPadding READ bottomPadding NOTIFY updated)

    Q_PROPERTY(qreal width READ width NOTIFY updated)
    Q_PROPERTY(qreal height READ height NOTIFY updated)

    Q_PROPERTY(qreal borderWidth READ borderWidth NOTIFY updated)
    Q_PROPERTY(QColor borderColor READ borderColor NOTIFY updated)

    Q_PROPERTY(QColor shadowColor READ shadowColor NOTIFY updated)
    Q_PROPERTY(qreal shadowSize READ shadowSize NOTIFY updated)
    Q_PROPERTY(qreal shadowXOffset READ shadowXOffset NOTIFY updated)
    Q_PROPERTY(qreal shadowYOffset READ shadowYOffset NOTIFY updated)

    Q_PROPERTY(qreal topLeftRadius READ topLeftRadius NOTIFY updated)
    Q_PROPERTY(qreal topRightRadius READ topRightRadius NOTIFY updated)
    Q_PROPERTY(qreal bottomLeftRadius READ bottomLeftRadius NOTIFY updated)
    Q_PROPERTY(qreal bottomRightRadius READ bottomRightRadius NOTIFY updated)

    Q_PROPERTY(QFont font READ font NOTIFY updated)

public:
    using Ptr = std::shared_ptr<StyleElement>;

    StyleElement(QObject *parent = nullptr);

    Q_SIGNAL void updated();

    QColor color() const;
    QColor backgroundColor() const;

    qreal leftPadding() const;
    qreal rightPadding() const;
    qreal topPadding() const;
    qreal bottomPadding() const;

    qreal width() const;
    qreal height() const;

    bool hasBorder() const;
    QColor borderColor() const;
    qreal borderWidth() const;

    bool hasShadow() const;
    QColor shadowColor() const;
    qreal shadowSize() const;
    qreal shadowXOffset() const;
    qreal shadowYOffset() const;

    qreal topLeftRadius() const;
    qreal topRightRadius() const;
    qreal bottomLeftRadius() const;
    qreal bottomRightRadius() const;

    QFont font() const;

    std::optional<QColor> m_color;
    std::optional<QColor> m_backgroundColor;

    std::optional<qreal> m_leftPadding;
    std::optional<qreal> m_rightPadding;
    std::optional<qreal> m_topPadding;
    std::optional<qreal> m_bottomPadding;

    std::optional<qreal> m_width;
    std::optional<qreal> m_height;

    std::optional<QColor> m_borderColor;
    std::optional<qreal> m_borderWidth;

    std::optional<QColor> m_shadowColor;
    std::optional<qreal> m_shadowSize;
    std::optional<qreal> m_shadowXOffset;
    std::optional<qreal> m_shadowYOffset;

    std::optional<qreal> m_topLeftRadius;
    std::optional<qreal> m_topRightRadius;
    std::optional<qreal> m_bottomLeftRadius;
    std::optional<qreal> m_bottomRightRadius;

    std::optional<QFont> m_font;

    std::shared_ptr<StyleElement> m_inherit;
};

#endif // STYLEDELEMENT_H
