/*
 * SPDX-FileCopyrightText: 2020 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#include "StyleElement.h"

#include <QDebug>
#include <QGuiApplication>

StyleElement::StyleElement(QObject *parent)
    : QObject(parent)
{
}

QColor StyleElement::color() const
{
    if (m_color) {
        return m_color.value();
    }

    if (m_inherit) {
        return m_inherit->color();
    }

    return Qt::black;
}

QColor StyleElement::backgroundColor() const
{
    if (m_backgroundColor) {
        return m_backgroundColor.value();
    }

    if (m_inherit) {
        return m_inherit->backgroundColor();
    }

    return Qt::white;
}

qreal StyleElement::leftPadding() const
{
    if (m_leftPadding) {
        return m_leftPadding.value();
    }

    if (m_inherit) {
        return m_inherit->leftPadding();
    }

    return 0.0;
}

qreal StyleElement::rightPadding() const
{
    if (m_rightPadding) {
        return m_rightPadding.value();
    }

    if (m_inherit) {
        return m_inherit->rightPadding();
    }

    return 0.0;
}

qreal StyleElement::topPadding() const
{
    if (m_topPadding) {
        return m_topPadding.value();
    }

    if (m_inherit) {
        return m_inherit->topPadding();
    }

    return 0.0;
}

qreal StyleElement::bottomPadding() const
{
    if (m_bottomPadding) {
        return m_bottomPadding.value();
    }

    if (m_inherit) {
        return m_inherit->bottomPadding();
    }

    return 0.0;
}

qreal StyleElement::width() const
{
    if (m_width) {
        return m_width.value();
    }

    if (m_inherit) {
        return m_inherit->width();
    }

    return 0.0;
}

qreal StyleElement::height() const
{
    if (m_height) {
        return m_height.value();
    }

    if (m_inherit) {
        return m_inherit->height();
    }

    return 0.0;
}

bool StyleElement::hasBorder() const
{
    if (m_borderColor || m_borderWidth) {
        return true;
    }
    if (m_inherit) {
        return m_inherit->hasBorder();
    }
    return false;
}

QColor StyleElement::borderColor() const
{
    if (m_borderColor) {
        return m_borderColor.value();
    }

    if (m_inherit) {
        return m_inherit->borderColor();
    }

    return Qt::black;
}

qreal StyleElement::borderWidth() const
{
    if (m_borderWidth) {
        return m_borderWidth.value();
    }

    if (m_inherit) {
        return m_inherit->borderWidth();
    }

    return 0.0;
}

bool StyleElement::hasShadow() const
{
    if (m_shadowColor || m_shadowSize || m_shadowXOffset || m_shadowYOffset) {
        return true;
    }
    if (m_inherit) {
        return m_inherit->hasShadow();
    }
    return false;
}


QColor StyleElement::shadowColor() const
{
    if (m_shadowColor) {
        return m_shadowColor.value();
    }

    if (m_inherit) {
        return m_inherit->shadowColor();
    }

    return Qt::transparent;
}

qreal StyleElement::shadowSize() const
{
    if (m_shadowSize) {
        return m_shadowSize.value();
    }

    if (m_inherit) {
        return m_inherit->shadowSize();
    }

    return 0.0;
}

qreal StyleElement::shadowXOffset() const
{
    if (m_shadowXOffset) {
        return m_shadowXOffset.value();
    }

    if (m_inherit) {
        return m_inherit->shadowXOffset();
    }

    return 0.0;
}

qreal StyleElement::shadowYOffset() const
{
    if (m_shadowYOffset) {
        return m_shadowYOffset.value();
    }

    if (m_inherit) {
        return m_inherit->shadowYOffset();
    }

    return 0.0;
}

qreal StyleElement::topLeftRadius() const
{
    if (m_topLeftRadius) {
        return m_topLeftRadius.value();
    }

    if (m_inherit) {
        return m_inherit->topLeftRadius();
    }

    return 0.0;
}

qreal StyleElement::topRightRadius() const
{
    if (m_topRightRadius) {
        return m_topRightRadius.value();
    }

    if (m_inherit) {
        return m_inherit->topRightRadius();
    }

    return 0.0;
}

qreal StyleElement::bottomLeftRadius() const
{
    if (m_bottomLeftRadius) {
        return m_bottomLeftRadius.value();
    }

    if (m_inherit) {
        return m_inherit->bottomLeftRadius();
    }

    return 0.0;
}

qreal StyleElement::bottomRightRadius() const
{
    if (m_bottomRightRadius) {
        return m_bottomRightRadius.value();
    }

    if (m_inherit) {
        return m_inherit->bottomRightRadius();
    }

    return 0.0;
}

QFont StyleElement::font() const
{
    if (m_font) {
        return m_font.value();
    }

    if (m_inherit) {
        return m_inherit->font();
    }

    return qGuiApp->font();
}
