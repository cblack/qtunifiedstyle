/*
 * SPDX-FileCopyrightText: 2020 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#include "Style.h"

#include <QStandardPaths>
#include <QUrl>

#include <QDebug>

#include "input/css/CssLoader.h"

class Style::Private
{
public:
    std::unique_ptr<CssLoader> styleLoader;
};

Style::Style()
    : QObject(nullptr)
    , d(std::make_unique<Private>())
{
    d->styleLoader = std::make_unique<CssLoader>();

    auto path = QStandardPaths::locate(QStandardPaths::GenericDataLocation, QStringLiteral("unifiedstyle/default.css"));
    d->styleLoader->load(QUrl::fromLocalFile(path));
}

Style::~Style() = default;

std::shared_ptr<StyleElement> Style::get(const QString &elementName, const QString &subElementName)
{
    return d->styleLoader->get(elementName, subElementName);
}

std::shared_ptr<Style> Style::instance()
{
    static std::shared_ptr<Style> inst = std::make_shared<Style>();
    return inst;
}

