/*
 * SPDX-FileCopyrightText: 2020 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#pragma once

#include "StyleElement.h"

class StyleElementQml : public StyleElement
{
    Q_OBJECT
    Q_PROPERTY(QString elementName READ elementName WRITE setElementName NOTIFY elementNameChanged)
    Q_PROPERTY(QString subElementName READ subElementName WRITE setSubElementName NOTIFY subElementNameChanged)

public:
    StyleElementQml(QObject *parent = nullptr);

    QString elementName() const;
    void setElementName(const QString &newElementName);
    Q_SIGNAL void elementNameChanged();

    QString subElementName() const;
    void setSubElementName(const QString &newSubElementName);
    Q_SIGNAL void subElementNameChanged();

private:
    void updateElement();

    QString m_elementName;
    QString m_subElementName;
};
