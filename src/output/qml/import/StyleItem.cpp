/*
 * SPDX-FileCopyrightText: 2020 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#include "StyleItem.h"

#include <QSGSimpleRectNode>

#include "Style.h"
#include "StyleElement.h"

StyleItem::StyleItem(QQuickItem* parent)
    : QQuickItem(parent)
{
//     setFlag(QQuickItem::ItemHasContents, true);

    connect(this, &StyleItem::elementNameChanged, this, &StyleItem::styleElementChanged);
    connect(this, &StyleItem::hoverChanged, this, &StyleItem::styleElementChanged);
    connect(this, &StyleItem::focusChanged, this, &StyleItem::styleElementChanged);
    connect(this, &StyleItem::activeChanged, this, &StyleItem::styleElementChanged);
}

QString StyleItem::elementName() const
{
    return m_elementName;
}

void StyleItem::setElementName(const QString &newElementName)
{
    if (newElementName == m_elementName) {
        return;
    }

    m_elementName = newElementName;
    m_element = Style::instance()->get(m_elementName);
    m_activeElement = Style::instance()->get(m_elementName, QStringLiteral("active"));
    m_focusElement = Style::instance()->get(m_elementName, QStringLiteral("focus"));
    m_hoverElement = Style::instance()->get(m_elementName, QStringLiteral("hover"));

    update();
    Q_EMIT elementNameChanged();
}

StyleElement *StyleItem::styleElement() const
{
    if (m_active && m_activeElement) {
        return m_activeElement.get();
    }
    if (m_focus && m_focusElement) {
        return m_focusElement.get();
    }
    if (m_hover && m_hoverElement) {
        return m_hoverElement.get();
    }

    return m_element.get();
}

bool StyleItem::hover() const
{
    return m_hover;
}

void StyleItem::setHover(bool newHover)
{
    if (newHover == m_hover) {
        return;
    }

    m_hover = newHover;
    update();
    Q_EMIT hoverChanged();
}

bool StyleItem::focus() const
{
    return m_focus;
}

void StyleItem::setFocus(bool newFocus)
{
    if (newFocus == m_focus) {
        return;
    }

    m_focus = newFocus;
    update();
    Q_EMIT focusChanged();
}

bool StyleItem::active() const
{
    return m_active;
}

void StyleItem::setActive(bool newActive)
{
    if (newActive == m_active) {
        return;
    }

    m_active = newActive;
    update();
    Q_EMIT activeChanged();
}

QSGNode * StyleItem::updatePaintNode(QSGNode* node, QQuickItem::UpdatePaintNodeData* data)
{
//     Q_UNUSED(data)
//
//     if (!node) {
//         node = new QSGSimpleRectNode{};
//     }
//
//     if (m_element->m_width > 0.0) {
//         setImplicitWidth(m_element->m_width);
//     }
//     if (m_element->m_height > 0.0) {
//         setImplicitHeight(m_element->m_height);
//     }
// //     setImplicitSize(m_element->m_width, m_element->m_height);
//
//     auto rectNode = static_cast<QSGSimpleRectNode*>(node);
//
//     rectNode->setRect(boundingRect());
//     rectNode->setColor(m_element->m_backgroundColor);
//
//     return rectNode;
    return nullptr;
}
