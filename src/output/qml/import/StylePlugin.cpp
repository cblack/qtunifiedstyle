/*
 * SPDX-FileCopyrightText: 2020 Arjen Hiemstra <ahiemstra@heimr.nl>
 *
 * SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#include "StylePlugin.h"

#include "StyleItem.h"
#include "StyleElementQml.h"

StylePlugin::StylePlugin(QObject *parent)
    : QQmlExtensionPlugin(parent)
{
}

void StylePlugin::registerTypes(const char *uri)
{
    Q_ASSERT(QString::fromLatin1(uri) == QLatin1String("org.kde.unifiedstyle"));

    qmlRegisterUncreatableType<StyleElement>(uri, 1, 0, "StyleElementBase", QStringLiteral("Base class for StyleElement"));
    qmlRegisterType<StyleItem>(uri, 1, 0, "StyleItem");
    qmlRegisterType<StyleElementQml>(uri, 1, 0, "StyleElement");
}
