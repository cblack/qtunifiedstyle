/*
 * SPDX-FileCopyrightText: 2020 Arjen Hiemstra <ahiemstra@heimr.nl>
 *
 * SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#ifndef CSSPLUGIN_H
#define CSSPLUGIN_H

#include <QQmlExtensionPlugin>

class StylePlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface")

public:
    explicit StylePlugin(QObject *parent = nullptr);
    void registerTypes(const char *uri) override;
};

#endif // CSSPLUGIN_H
