/*
 * SPDX-FileCopyrightText: 2020 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#include "StyleElementQml.h"

#include "Style.h"

StyleElementQml::StyleElementQml(QObject* parent)
    : StyleElement(parent)
{
}

QString StyleElementQml::elementName() const
{
    return m_elementName;
}

void StyleElementQml::setElementName(const QString &newElementName)
{
    if (newElementName == m_elementName) {
        return;
    }

    m_elementName = newElementName;
    updateElement();
    Q_EMIT elementNameChanged();
}

QString StyleElementQml::subElementName() const
{
    return m_subElementName;
}

void StyleElementQml::setSubElementName(const QString & newSubElementName)
{
    if (newSubElementName == m_subElementName) {
        return;
    }

    m_subElementName = newSubElementName;
    updateElement();
    Q_EMIT subElementNameChanged();
}

void StyleElementQml::updateElement()
{
    auto element = Style::instance()->get(m_elementName, m_subElementName);
    if (!element) {
        return;
    }

    m_inherit = element;
    Q_EMIT updated();
}
