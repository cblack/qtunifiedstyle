
import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Templates 2.15 as T

import org.kde.kirigami 2.12 as Kirigami

import org.kde.unifiedstyle 1.0 as Unified

T.TextField {
    id: control

    implicitWidth: implicitBackgroundWidth + leftInset + rightInset
                   || Math.max(contentWidth, placeholder.implicitWidth) + leftPadding + rightPadding
    implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
                             contentHeight + topPadding + bottomPadding,
                             placeholder.implicitHeight + topPadding + bottomPadding)

    leftPadding: styleItem.style.leftPadding
    rightPadding: styleItem.style.rightPadding
    topPadding: styleItem.style.topPadding
    bottomPadding: styleItem.style.bottomPadding

    color: styleItem.style.color
    selectionColor: selectionStyle.backgroundColor
    selectedTextColor: selectionStyle.color
    placeholderTextColor: placeholderStyle.color
//     verticalAlignment: styleItem.style.verticalAlignment
//     horizontalAlignment: styleItem.style.horizontalAlignment
    font: styleItem.style.font

    Text {
        id: placeholder
        x: control.leftPadding
        y: control.topPadding
        width: control.width - (control.leftPadding + control.rightPadding)
        height: control.height - (control.topPadding + control.bottomPadding)

        text: control.placeholderText
        font: placeholderStyle.font
        color: placeholderStyle.color
        horizontalAlignment: control.horizontalAlignment
        verticalAlignment: control.verticalAlignment
        visible: !control.length && !control.preeditText && (!control.activeFocus || control.horizontalAlignment !== Qt.AlignHCenter)
        elide: Text.ElideRight
    }

    background: Kirigami.ShadowedRectangle {
        implicitWidth: styleItem.style.width
        implicitHeight: styleItem.style.height

        color: styleItem.style.backgroundColor

        corners.topLeftRadius: styleItem.style.topLeftRadius
        corners.topRightRadius: styleItem.style.topRightRadius
        corners.bottomLeftRadius: styleItem.style.bottomLeftRadius
        corners.bottomRightRadius: styleItem.style.bottomRightRadius

        border.width: styleItem.style.borderWidth
        border.color: styleItem.style.borderColor

        shadow.size: styleItem.style.shadowSize
        shadow.color: styleItem.style.shadowColor
        shadow.xOffset: styleItem.style.shadowXOffset
        shadow.yOffset: styleItem.style.shadowYOffset
    }

    Unified.StyleItem {
        id: styleItem
        elementName: "textfield"

        hover: control.hovered
        focus: control.focus
    }

    Unified.StyleElement {
        id: selectionStyle
        elementName: "textfield"
        subElementName: "selection"
    }

    Unified.StyleElement {
        id: placeholderStyle
        elementName: "textfield"
        subElementName: "placeholder"
    }
}
