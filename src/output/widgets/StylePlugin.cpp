/*
 * SPDX-FileCopyrightText: 2020 Arjen Hiemstra <ahiemstra@heimr.nl>
 *
 * SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#include "StylePlugin.h"

#include "UnifiedStyle.h"

StylePlugin::StylePlugin(QObject *parent)
    : QStylePlugin(parent)
{
}


QStyle *StylePlugin::create(const QString &key)
{
    if (key.toLower() == QStringLiteral("unified")) {
        return new UnifiedStyle();
    }

    return nullptr;
}
