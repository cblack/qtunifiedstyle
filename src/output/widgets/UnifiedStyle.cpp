/*
 * SPDX-FileCopyrightText: 2020 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#include "UnifiedStyle.h"

#include <QStyleOption>
#include <QPainter>
#include <QDebug>

#include "StyleElement.h"
#include "Style.h"

UnifiedStyle::UnifiedStyle()
    : QProxyStyle(QStringLiteral("fusion"))
{
}

QSize UnifiedStyle::sizeFromContents(QStyle::ContentsType ct, const QStyleOption* opt, const QSize& contentsSize, const QWidget* w) const
{
    if (ct == QStyle::CT_PushButton) {
        const auto buttonOption = qstyleoption_cast<const QStyleOptionButton*>(opt);

        StyleElement::Ptr element;
        if (buttonOption->state & (QStyle::State_On | QStyle::State_Sunken)) {
            element = Style::instance()->get(QStringLiteral("button"), QStringLiteral("active"));
        } else if (buttonOption->state & QStyle::State_HasFocus) {
            element = Style::instance()->get(QStringLiteral("button"), QStringLiteral("focus"));
        } else if (buttonOption->state & QStyle::State_MouseOver) {
            element = Style::instance()->get(QStringLiteral("button"), QStringLiteral("hover"));
        } else {
            element = Style::instance()->get(QStringLiteral("button"));
        }

        auto size = contentsSize.grownBy({
            int(element->leftPadding()),
            int(element->topPadding()),
            int(element->rightPadding()),
            int(element->bottomPadding())
        });
        size = size.grownBy({
            int(element->borderWidth()),
            int(element->borderWidth()),
            int(element->borderWidth()),
            int(element->borderWidth())
        });
        size = size.grownBy({
            int(std::max(element->shadowSize() - element->shadowXOffset(), 0.0)),
            int(std::max(element->shadowSize() - element->shadowYOffset(), 0.0)),
            int(element->shadowSize() + element->shadowXOffset()),
            int(element->shadowSize() + element->shadowYOffset())
        });
        return size;
    }

    return QProxyStyle::sizeFromContents(ct, opt, contentsSize, w);
}

void UnifiedStyle::drawControl(QStyle::ControlElement element, const QStyleOption *opt, QPainter *p, const QWidget *w) const
{
    if (element == QStyle::CE_PushButton) {
        drawPushButton(opt, p, w);
        return;
    }

    QProxyStyle::drawControl(element, opt, p, w);
}

void UnifiedStyle::drawPushButton(const QStyleOption *opt, QPainter *painter, const QWidget *widget) const
{
    const auto buttonOption = qstyleoption_cast<const QStyleOptionButton*>(opt);

    StyleElement::Ptr element;
    if (buttonOption->state & (QStyle::State_On | QStyle::State_Sunken)) {
        element = Style::instance()->get(QStringLiteral("button"), QStringLiteral("active"));
    } else if (buttonOption->state & QStyle::State_HasFocus) {
        element = Style::instance()->get(QStringLiteral("button"), QStringLiteral("focus"));
    } else if (buttonOption->state & QStyle::State_MouseOver) {
        element = Style::instance()->get(QStringLiteral("button"), QStringLiteral("hover"));
    } else {
        element = Style::instance()->get(QStringLiteral("button"));
    }

    auto radius = element->topLeftRadius();

    auto controlRect = opt->rect;

    if (element->hasShadow()) {
        painter->setBrush(QBrush(element->shadowColor()));
        painter->setPen(element->shadowColor());
        painter->drawRoundedRect(buttonOption->rect, radius, radius);

        controlRect.adjust(std::max(element->shadowSize() - element->shadowXOffset(), 0.0),
                           std::max(element->shadowSize() - element->shadowYOffset(), 0.0),
                           -(element->shadowSize() + element->shadowXOffset()),
                           -(element->shadowSize() + element->shadowYOffset()));
    }

    painter->setBrush(QBrush(element->backgroundColor()));
    painter->setPen(QPen(element->borderColor(), element->borderWidth()));
    painter->drawRoundedRect(controlRect, element->topLeftRadius(), element->topLeftRadius());

    painter->setFont(element->font());
    painter->setPen(element->color());
    drawItemText(painter, controlRect, Qt::AlignCenter, buttonOption->palette, true, buttonOption->text);
}

