import QtQuick 2.15
import QtQuick.Controls 2.15

ApplicationWindow {
    width: 800
    height: 600
    visible: true

    Column {
        anchors.centerIn: parent
        spacing: 10

        Button {
            text: "Example"
        }

        TextField {
            placeholderText: "Example"
        }
    }
}
