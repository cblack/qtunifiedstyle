
find_library(Katana_LIBRARIES
    NAMES katana
)

find_path(Katana_INCLUDE_DIRS
    NAMES katana.h
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Katana DEFAULT_MSG Katana_LIBRARIES Katana_INCLUDE_DIRS)
